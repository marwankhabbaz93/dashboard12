const baseApiUrl = 'http://foxapp.gotdns.com';

export const environment = {
  name: 'Dev',
  version: '1.0.0',
  production: false,
  baseApiUrl: baseApiUrl,
};

