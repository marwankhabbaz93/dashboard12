const baseApiUrl = 'http://foxapp.gotdns.com';

export const environment = {
  name: 'Prod',
  version: '1.0.0',
  production: true,
  baseApiUrl: baseApiUrl,
};
