import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { environment } from 'src/environments/environment';
import axios from 'axios';

@Injectable()
export class ApiService {

  constructor( private router: Router,
    private toastr: ToastrService,) {
   }

  loadConfig(alternateBaseUrl: string = "") {
    axios.defaults.headers.common['App-Name'] = environment.name;
    axios.defaults.headers.common['App-Version'] = environment.version;
    if(alternateBaseUrl){
      axios.defaults.baseURL = alternateBaseUrl;
    }else{
      axios.defaults.baseURL = environment.baseApiUrl;
    }
    axios.defaults.headers.common['x-api-key'] = 'X';
    axios.defaults.headers.common['x-blob-name'] = 'CodeGeekIr';
    axios.defaults.headers.common['x-container-name'] = 'CodeGeek';
  }

  async get(url: string, proms = {}, isAuthorized: boolean = false, successMessage?: string, showErrorToaster: boolean = true, alternateBaseUrl: string = "") {

    this.loadConfig(alternateBaseUrl);

    return await axios.get(url, { params: proms })
      .then((response) => {
        if (successMessage) {
          this.toastr.success(successMessage);
        }
        return response.data;
      })
      .catch((error) => {
        if (showErrorToaster) {
          this.toastr.error(error);
        }
      });
  };

  async post(url: string, data = {}, proms = {}, isAuthorized: boolean = false, successMessage?: string, showErrorToaster: boolean = true) {

    this.loadConfig();

    return await axios.post(url, data, { params: proms })
      .then((response) => {
        if (successMessage) {
          this.toastr.success(successMessage);
        }
        return response.data;
      })
      .catch((error) => {
        if (showErrorToaster) {
          this.toastr.error(error);
        }
      });
  };

  async pull(url: string, proms = {}, isAuthorized: boolean = false, successMessage?: string, showErrorToaster: boolean = true) {

    this.loadConfig();

    return await axios.post(url, proms)
      .then((response) => {
        if (successMessage) {
          this.toastr.success(successMessage);
        }
        return response.data;
      })
      .catch((error) => {
        if (showErrorToaster) {
          this.toastr.error(error);
        }
      });
  };

  async delete(url: string, proms = {}, isAuthorized: boolean = false, successMessage?: string, showErrorToaster: boolean = true) {

    this.loadConfig();

    return await axios.delete(url, proms)
      .then((response) => {
        if (successMessage) {
          this.toastr.success(successMessage);
        }
        return response.data;
      })
      .catch((error) => {
        if (showErrorToaster) {
          this.toastr.error(error);
        }
      });
  };

  // showToaster(param: any, isSuccess: boolean = true, isAuthorized: boolean = false, showErrorToaster: boolean = true) {
  //   //success flow .. param is string message to show
  //   if (isSuccess) {
  //     return this.toastr.success(param, 'Success');
  //   }
  //   //error flow .. param is error object
  //   var status = param.request.status;

  //   // Request made and server responded
  //   if (param.response) {
  //     //assign the response code to be handeled
  //     status = param.response.status;
  //     //reroute to login page if authorization is expected
  //     if (isAuthorized && status === 401) {
  //       this.router.navigate(['/login']);
  //     }
  //     //response is string - pass it to toaster message
  //     if (typeof param.response.data === 'string') {
  //       return this.toastr.error(param.response.data, param.response.status);
  //     }
  //   }

  //   //no response from server .. handle http codes
  //   switch (status) {
  //     case 400: {
  //       return this.toastr.error('Bad Request !', param.name);
  //     }
  //     case 401: {
  //       //reroute to login page if authorization is expected
  //       if (isAuthorized) {
  //         this.router.navigate(['/login']);
  //       }
  //       return this.toastr.error('Unauthorized !', param.name);
  //     }
  //     case 404: {
  //       return this.toastr.error('Not Found !', param.name);
  //     }
  //     case 500: {
  //       return this.toastr.error('Something went wrong! Please try again later', param.name);
  //     }
  //     default: {
  //       return this.toastr.error(param.message, param.name);
  //     }
  //   }
  // }
}
