
import { ApiService } from './api-service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class fileService {

  constructor(public apiService: ApiService) {
  }
  // get Transactions
  getFileTransactions(pageNumber: number = 1, pageSize: number = 20) {
    const params = {
      pageNumber: pageNumber,
      pageSize: pageSize,
    };
    return this.apiService.get(`api/v1/file-transactions`, params);
  }

  // upload file
  uploadFile(file: any): any {
    return this.apiService.post(`api/v1/files/upload`, file);
  }

  // upload files
  uploadFiles(data: []) {
    return this.apiService.post(`api/v1/files/upload/multiple`, { data });
  }

  // delete file bu ID
  deleteFileById(id: string) {
    return this.apiService.delete(`api/v1/files/id/${id}`)
  }

  // delete file by name
  deleteFileByName(name: string) {
    return this.apiService.delete(`api/v1/files/fileName/${name}`)
  }

  // soft file by name
  softFileById(id: string) {
    return this.apiService.delete(`api/v1/files/soft/id/${id}`)
  }

  //soft file by name
  softFileByName(name: string) {
    return this.apiService.delete(`api/v1/files/soft/fileName/${name}`);
  }

  // undo file by id
  undoFileById(id: string) {
    return this.apiService.delete(`api/v1/files/soft/undo/id/${id}`);
  }

  // undo file by name
  undoFileByName(name: string) {
    return this.apiService.delete(`api/v1/files/soft/undo/fileName/${name}`);
  }

  // get Services
  getServices() {
    return this.apiService.get(`api/v1/services`);
  }

  // post Services
  postServices() {
    return this.apiService.post(`api/v1/services`);
  }

  // get Service by ID 
  getServiceById(id: string) {
    return this.apiService.get(`api/v1/services/id/${id}`)
  }

  //delete service by ID
  deleteServiceById(id: string) {
    return this.apiService.delete(`api/v1/services/id/${id}`)
  }

  //soft delete service by ID
  softDeleteServiceById(id: string) {
    return this.apiService.delete(`api/v1/services/soft/id/${id}`)
  }

  //undo delete service by name
  softDeleteServiceByName(id: string) {
    return this.apiService.delete(`api/v1/services/soft/undo/id/${id}`)
  }
}