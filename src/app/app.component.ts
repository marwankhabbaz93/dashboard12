import { Component } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
declare var $ : any
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'file-dashbaord';
  public languages = ['en', 'ar', 'per'];
  textDir: string;
  isLeft: boolean = false; 

  constructor(
    private translate: TranslateService,
  ) {
    if (localStorage.getItem('language') === null) {
      localStorage.setItem('language', 'en');
    }

    translate.setDefaultLang(window.localStorage.getItem('language'));

    localStorage.getItem("rtl");


    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      switch (event.lang) {
        case "ar": {
          window.location.reload();
          this.textDir = 'rtl';
          this.isLeft = true;
          localStorage.setItem("rtl", this.textDir);
          break;
        }

        case "per": {
          window.location.reload();
          this.textDir = 'rtl';
          this.isLeft = true;
          localStorage.setItem("rtl", this.textDir);
          break;
        }
        default:
          window.location.reload();
          this.textDir = 'ltr';
          this.isLeft = false;
          localStorage.setItem("rtl", this.textDir);
      }

    });
  }

  ngOnInit() {
    this.translate.addLangs(this.languages);
    var dir = localStorage.getItem("rtl");
    this.textDir = dir;
    if (this.textDir === 'rtl'){
      var element = document.getElementById("dir");
       element.classList.add("rtl");
    }
  }
}
