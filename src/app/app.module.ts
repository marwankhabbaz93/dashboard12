import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgApexchartsModule } from 'ng-apexcharts';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { HeaderComponent } from './component/header/header.component';
import { SidebarListComponent } from './component/sidebar-list/sidebar-list.component';
import { DailyUsageComponent } from './component/daily-usage/daily-usage.component';
import { FavouritesComponent } from './component/favourites/favourites.component';
import { RecentActivitiesComponent } from './component/recent-activities/recent-activities.component';
import { QuickAccessComponent } from './component/quick-access/quick-access.component';
import { FooterComponent } from './component/footer/footer.component';
import { FilesComponent } from './files/files.component';
import { ActivitiesComponent } from './activities/activities.component';
import { UsersComponent } from './users/users.component';
import { ComponentsComponent } from './components/components.component';
import { SettingsComponent } from './settings/settings.component';
import { ReportComponent } from './component/report/report.component';
import { GetUpgradeComponent } from './component/get-upgrade/get-upgrade.component';
import { FilesDirectoryComponent } from './component/files-directory/files-directory.component';
import { SubActivityComponent } from './component/sub-activity/sub-activity.component';
import { SubSettingsComponent } from './component/sub-settings/sub-settings.component';

//Services 
import { ApiService } from './services/api-service';
import { fileService } from './services/file-service';
import { LoadingComponent } from './component/loading/loading.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    SidebarListComponent,
    DailyUsageComponent,
    FavouritesComponent,
    RecentActivitiesComponent,
    QuickAccessComponent,
    FooterComponent,
    FilesComponent,
    ActivitiesComponent,
    UsersComponent,
    ComponentsComponent,
    SettingsComponent,
    ReportComponent,
    GetUpgradeComponent,
    FilesDirectoryComponent,
    SubActivityComponent,
    SubSettingsComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgApexchartsModule,
    NgxSpinnerModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      timeOut: 5000,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ApiService,
    fileService,
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
