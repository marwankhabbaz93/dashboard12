import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-activity',
  templateUrl: './sub-activity.component.html',
  styleUrls: ['./sub-activity.component.scss']
})
export class SubActivityComponent implements OnInit {
  public isLeft: boolean = false;

  constructor() { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("rtl1");

    }
  }

}
