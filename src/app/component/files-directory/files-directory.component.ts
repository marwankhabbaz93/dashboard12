import { Component, OnInit } from '@angular/core';
declare var $ : any;

@Component({
  selector: 'app-files-directory',
  templateUrl: './files-directory.component.html',
  styleUrls: ['./files-directory.component.scss']
})
export class FilesDirectoryComponent implements OnInit {

    public isLeft: boolean = false;

  constructor() { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir1");
      element.classList.add("rtl1");
      var element = document.getElementById("dir2");
      element.classList.add("rtl2");
    }

    var jsonData = {
      'data': [
          {
              'text': 'Backups',
              'type': 'folder',
              'children': [
                  {
                      'text': '2020/5/2',
                      'type': 'folder'
                  },
                  {
                      'text': '2020/2/7',
                      'type': 'folder'
                  },
                  {
                      'text': '2020/7/1',
                      'type': 'folder'
                  }
              ]
          },
          {
              'text': 'Documents',
              'type': 'folder',
              'state': {
                  'opened': true,
                  'selected': true
              },
              'children': [
                  {
                      'text': 'Design Thinking Project',
                      'type': 'folder'
                  },
                  {
                      'text': 'User Research',
                      'type': 'folder'
                  },
                  {
                      'text': 'Important Documents',
                      'type': 'folder'
                  }
              ]
          },
          {
              'text': 'Music',
              'type': 'folder',
              'children': [
                  {
                      'text': 'Rock',
                      'type': 'folder',
                  },
                  {
                      'text': 'Classic',
                      'type': 'folder',
                  },
                  {
                      'text': 'Blues',
                      'type': 'folder',
                  }
              ]
          },
          {
              'text': 'Pictures',
              'type': 'folder',
              'children': [
                  {
                      'text': 'Child 1',
                      'type': 'folder',
                  },
                  {
                      'text': 'Child 2',
                      'type': 'folder',
                  }
              ]
          },
          {
              'text': 'Downloads',
              'type': 'folder',
              'children': [
                  {
                      'text': 'Child 1',
                      'type': 'folder',
                  },
                  {
                      'text': 'Child 2',
                      'type': 'folder',
                  }
              ]
          },
      ],
      themes: {
          dots: false
      }
  };

  $('#files').jstree({
      'core': jsonData,
      "types": {
          "folder": {
              "icon": "fa fa-folder text-warning",
          },
          "file": {
              "icon": "fa fa-arrow-right",
          }
      },
      plugins: ["types"]
  });

  }
}
