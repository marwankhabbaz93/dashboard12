import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  selectedLang: string = '';
  public isLeft: boolean = false;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("rtl");
    }

    var selectedItem = sessionStorage.getItem("SelectedItem");
    $('#dropdown').val(selectedItem);

    $('#dropdown').change(function () {
      var dropVal = $(this).val();
      sessionStorage.setItem("SelectedItem", dropVal);
    });
  }

  selectChangeHandler(event: any) {
    this.selectedLang = event.target.value;
    this.translate.setDefaultLang(this.selectedLang);
    this.translate.use(this.selectedLang);
    localStorage.setItem("language", this.selectedLang);
  }
}
