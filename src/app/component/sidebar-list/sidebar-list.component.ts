import { Component, OnInit } from '@angular/core';
import { fileService } from '../../services/file-service';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

declare var $: any;
declare var JustGage: any;
@Component({
  selector: 'app-sidebar-list',
  templateUrl: './sidebar-list.component.html',
  styleUrls: ['./sidebar-list.component.scss']
})
export class SidebarListComponent implements OnInit {
  public isLeft: boolean = false;
  public fileToUpload: File = null;
  public base64textString: any;
  public group: FormGroup;
  public fileName: string = '';
  public url: string = '';
  public field: any;


  constructor(private fileService: fileService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("rtl");
    }

    if ($('#justgage_five').length) {
      new JustGage({
        id: 'justgage_five',
        value: 39,
        minTxt: " ",
        min: 0,
        max: 100,
        maxTxt: " ",
        symbol: '%',
        label: "Storage Usage",
        gaugeWidthScale: 0.7,
        counter: true,
        relativeGaugeSize: true,
        levelColors: ['#4c62df'],
        valueFontFamily: 'Josefin Sans'
      });

    }
  }

  public async onChange(event: any): Promise<void>  {
    const files = event.target.files;
    const formData: FormData = new FormData();

    if (files.length > 0) {
      const file = files[0];
      formData.append('file', file);
     await this.fileService.uploadFile(formData);
      this.toastr.success('The file was uploaded successfully!');
    }
    else
      this.toastr.error('Please try again later!');
  }

  public upload() {
    $('#file').focus().trigger('click');
  }

}