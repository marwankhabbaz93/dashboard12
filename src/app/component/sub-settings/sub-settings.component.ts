import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-settings',
  templateUrl: './sub-settings.component.html',
  styleUrls: ['./sub-settings.component.scss']
})
export class SubSettingsComponent implements OnInit {
  public isLeft: boolean = false;

  constructor() { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("rtl1");
  }
  }
}
