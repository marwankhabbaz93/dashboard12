import { Component, OnInit } from '@angular/core';
declare var ApexCharts: any;

@Component({
  selector: 'app-daily-usage',
  templateUrl: './daily-usage.component.html',
  styleUrls: ['./daily-usage.component.scss']
})
export class DailyUsageComponent implements OnInit {

    public isLeft: boolean = false;

  constructor() { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl'){
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("rtl");
    }
    var element = document.getElementById("dir");
    element.classList.add("ltr");

  this.dailyUsage();
  }

  public dailyUsage() {
    var options = {
        series: [{
            name: 'Uploads',
            data: [90, 70, 57, 56, 61, 100, 63, 60, 66]
        }, {
            name: 'Downloads',
            data: [49, 60, 101, 98, 87, 58, 91, 114, 94]
        }],
        chart: {
            type: 'bar',
            offsetX: -18,
            height: 390,
            width: '103%',
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 10,
            colors: ['transparent']
        },
        xaxis: {
            categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val + "GB"
                }
            }
        },
        legend: {
            show: false
        }
    };

    var chart = new ApexCharts(document.querySelector("#daily-usage"), options);

    chart.render();
}
}
