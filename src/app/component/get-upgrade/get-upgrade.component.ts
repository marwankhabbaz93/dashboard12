import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-get-upgrade',
  templateUrl: './get-upgrade.component.html',
  styleUrls: ['./get-upgrade.component.scss']
})
export class GetUpgradeComponent implements OnInit {
  public isLeft: boolean = false;

  constructor() { }

  ngOnInit() {
    var dir = localStorage.getItem("rtl");
    if (dir === 'rtl') {
      this.isLeft = true;
      var element = document.getElementById("dir");
      element.classList.add("margin-right");
    }
  }

}
