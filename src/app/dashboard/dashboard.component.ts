import { Component, OnInit } from '@angular/core';
import {fileService} from '../../app/services/file-service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor( public fileService: fileService
  ) { }

  ngOnInit() {
    // this.fileService.getFileTransactions();
    // this.fileService.uploadFile('c048976b-fe9a-4792-9b75-f9562e15f133.PNG');
  }
}
